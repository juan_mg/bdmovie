# PracticeBDMovie
Proyecto de practica con API https://www.themoviedb.org/

# Caracteristicas
Lenguaje Kotlin
Arq MVVM

# Librerias 
//ImagenesCirculares
    implementation 'de.hdodenhof:circleimageview:3.1.0'
//Retrofit
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation "com.squareup.okhttp3:logging-interceptor:4.9.0"
    implementation 'com.google.code.gson:gson:2.8.6'
    implementation 'com.facebook.stetho:stetho-okhttp3:1.5.1'
//RoomDB
    implementation "androidx.room:room-runtime:2.3.0"
    kapt "androidx.room:room-compiler:2.3.0"
    implementation "androidx.room:room-ktx:$room_version"
    testImplementation "androidx.room:room-testing:$room_version"
//Glide
    implementation 'com.github.bumptech.glide:glide:4.12.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.12.0'
//Lottie
    implementation "com.airbnb.android:lottie:3.6.1"
//Animaciones
    implementation 'com.github.mohammadatif:Animatoo:master'
//Test
    testImplementation "androidx.arch.core:core-testing:2.1.0"
    testImplementation 'junit:junit:4.13.2'
    androidTestImplementation 'androidx.test:core:1.0.0'
// AndroidJUnitRunner and JUnit Rules
    androidTestImplementation 'androidx.test:runner:1.1.0'
    androidTestImplementation 'androidx.test:rules:1.1.0'
// Assertions
    androidTestImplementation 'androidx.test.ext:junit:1.0.0'
    androidTestImplementation 'androidx.test.ext:truth:1.0.0'
    androidTestImplementation 'com.google.truth:truth:0.42'
// Espresso dependencies
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-contrib:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-intents:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-accessibility:3.1.0'
    androidTestImplementation 'androidx.test.espresso:espresso-web:3.1.0'
    androidTestImplementation 'androidx.test.espresso.idling:idling-concurrent:3.1.0'
// AndroidX Test - JVM testing
    testImplementation "androidx.test.ext:junit-ktx:1.1.2"
    testImplementation "androidx.test:core-ktx:1.3.0"
    testImplementation "org.robolectric:robolectric:4.5.1"
# Requerimientos
1.-Películas y/o Series categorizadas por Popular y Top Rated
Completado
2.-Detalle de Película y/o Serie.
Completado
3.-Buscador de Películas y/o Series por nombre.
Completado
4.-Visualización de Videos en el detalle(). 
Completado
5.-La App debe poder funcionar offline(Libreria ROOM).
Completado
6._Truebas Unitarias.
Completado
7.-Transiciones/Animaciones.
Completado
La mayoría de los requerimientos se realizaron de la manera más eficaz y completos, excepto con el punto 6 que se hizo una prueba unitaria básica partir del punto 3 y punto 5 donde a base del texto de entrada se buscaran en todas las películas guardas la similitud con ese texto, es decir que si se recibe una entrada "a" si existen títulos con esa entrada mandara un mensaje "Hay similitudes"
