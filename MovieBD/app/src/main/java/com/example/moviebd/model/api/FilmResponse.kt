package com.example.moviebd.model.api

import com.example.moviebd.model.data.Film


data class FilmResponse(
    val page: Int? = null,
    val totalPages: Int? = null,
    val results: List<Film>? = null,
    val totalResults: Int? = null
)
