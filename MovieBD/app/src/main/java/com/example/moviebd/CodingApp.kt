package com.example.moviebd

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.example.moviebd.model.db.FilmDB
import com.facebook.stetho.Stetho

class CodingApp : Application(){
    override fun onCreate() {
        super.onCreate()
        context = this.applicationContext
        mApplication = this
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
        room = Room.databaseBuilder(this, FilmDB::class.java, "films").build()
    }

    companion object {
        private var context: Context? = null
        private var room: FilmDB? = null
        private var mApplication: Application?=null
        fun getContext() = context!!
        fun getInstance(): Application = mApplication!!
        fun getRoom() = room
    }
}