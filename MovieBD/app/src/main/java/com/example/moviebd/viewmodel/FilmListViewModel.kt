package com.example.moviebd.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviebd.R
import com.example.moviebd.model.data.Film
import com.example.moviebd.model.network.Resource
import com.example.moviebd.model.repositiry.FilmListRepositoryImpl
import com.example.moviebd.view.adapter.FilmItemAdapter

class FilmListViewModel(
    val listener: FilmItemAdapter.OnFilmClickListener
) : ViewModel() {
    private val repository = FilmListRepositoryImpl()

    lateinit var filmsSearched: MutableLiveData<List<Film>>

    var films: LiveData<Resource<List<Film>?>>

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> get() = _message

    private var filmsAdapter: FilmItemAdapter? = null

    private val _selectFilm = MutableLiveData<Film>()

    val selectFilm: LiveData<Film> get() = _selectFilm


    fun getFilmList(type: Int): LiveData<Resource<List<Film>?>> {
        _isLoading.value = true
        films = repository.getFilmList(type)
        return films
    }

    fun getFilmListByName(text: String, type: Int):LiveData<List<Film>> {
        val searches = repository.getFilmsByName(text, type)
        return searches
    }

    fun setFilmsInRecyclerView(list: List<Film>?) {
        _isLoading.value = false
        filmsSearched.value = list
        if (!list.isNullOrEmpty()) {
            filmsAdapter?.setFilmList(list)
            filmsAdapter?.notifyDataSetChanged()
        } else {
            filmsAdapter?.setFilmList(arrayListOf())
            filmsAdapter?.notifyDataSetChanged()
        }
    }

    fun getRecyclerFilmAdapter(): FilmItemAdapter? {
        filmsAdapter = FilmItemAdapter(this, R.layout.item_film, listener)
        return filmsAdapter
    }

    fun getFilmAtPosition(position: Int): Film? {
        return filmsSearched.value?.get(position)
    }

    fun selectFilm(position: Int): Film? {
        return filmsSearched.value?.get(position)
    }

    init {
        filmsAdapter = FilmItemAdapter(this, R.layout.item_film, listener)
        filmsSearched = MutableLiveData()
        films = repository.getFilmList(0)
    }


    companion object {
        private const val TAG = "FilmListViewModel"
    }

}