package com.example.moviebd.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SearchViewModel:ViewModel() {
    private val _textSearch = MutableLiveData<String>()
    val textSearch: LiveData<String> get() = _textSearch

    fun search(text : String){
        _textSearch.value = text
    }
}