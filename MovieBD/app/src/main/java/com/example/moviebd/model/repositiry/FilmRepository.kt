package com.example.moviebd.model.repositiry

import androidx.lifecycle.LiveData
import com.example.moviebd.model.data.Film
import com.example.moviebd.model.network.Resource

interface FilmRepository {
    fun getFilmList(type: Int): LiveData<Resource<List<Film>?>>
    fun getFilmsByName(text: String, type: Int): LiveData<List<Film>>
}