package com.example.moviebd.model.repositiry

import androidx.lifecycle.LiveData
import com.example.moviebd.CodingApp
import com.example.moviebd.model.api.ApiAdapter
import com.example.moviebd.model.api.ApiService
import com.example.moviebd.model.api.FilmResponse
import com.example.moviebd.model.data.Film
import com.example.moviebd.model.db.FilmDao
import com.example.moviebd.model.network.NetworkBoundResource
import com.example.moviebd.model.network.Resource
import retrofit2.Call


class FilmListRepositoryImpl() : FilmRepository {

    private val api: ApiService
    private val filmDao: FilmDao

    override fun getFilmList(type: Int): LiveData<Resource<List<Film>?>> {
        return object : NetworkBoundResource<List<Film>, FilmResponse>() {
            override fun saveCallResult(item: FilmResponse) {
                if (type == 0) {
                    item.results?.forEach {
                        it.is_top = true
                    }
                } else {
                    item.results?.forEach {
                        it.is_top = false
                    }
                }
                filmDao.inserFilm(item.results!!)
            }

            override fun loadFromDb(): LiveData<List<Film>> {
                if (type == 0) {
                    return filmDao.getFilmsTop()
                } else {
                    return filmDao.getFilmsPop()
                }
            }

            override fun createCall(): Call<FilmResponse> {
                if (type == 0) {
                    return api.getFilmsTopRate()
                } else {
                    return api.getFilmsPopular()
                }
            }
        }.asLiveData
    }

    override fun getFilmsByName(text: String, type: Int): LiveData<List<Film>> {
        val search = "%${text}%"
        return filmDao.searchByTitle(search)
    }

    init {
        filmDao = CodingApp.getRoom()?.filmDao()!!
        api = ApiAdapter.createService(ApiService::class.java)
    }
}