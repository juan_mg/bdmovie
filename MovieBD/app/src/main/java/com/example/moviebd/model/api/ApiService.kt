package com.example.moviebd.model.api

import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("popular?api_key=823a3fdbe5f98c58e07d310316e5373f&language=es-US&page=1")
    fun getFilmsPopular(): Call<FilmResponse>

    @GET("top_rated?api_key=823a3fdbe5f98c58e07d310316e5373f&language=es-US&page=1")
    fun getFilmsTopRate(): Call<FilmResponse>
}