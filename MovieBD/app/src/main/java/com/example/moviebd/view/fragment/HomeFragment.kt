package com.example.moviebd.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.example.moviebd.databinding.FragmentMenuBinding

class HomeFragment: Fragment() {
    private var _binding: FragmentMenuBinding? = null
    private val binding get() = _binding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMenuBinding.inflate(inflater, container, false).apply {
            viewLifecycleOwner
        }
        Animatoo.animateCard(activity)

        binding?.btnListPop?.setOnClickListener{
            val bundle = bundleOf("list" to "pop")
            NavHostFragment.findNavController(this)
//               .navigate(R.id.action_HomeFragment_to_PopularFragment,bundle)
        }

        binding?.btnListTop?.setOnClickListener{
            val bundle = bundleOf("list" to "top")
            NavHostFragment.findNavController(this)
//                .navigate(R.id.action_HomeFragment_to_TopRateFragment,bundle)
        }
        //
        //NavHostFragment.findNavController(this)
        //    .navigate(R.id.action_TopRate_to_Detail, bundle)

        return binding?.root
    }
}