package com.example.moviebd.model.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.moviebd.model.data.Film

@Dao
abstract class FilmDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(film: Film)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun inserFilm(film: List<Film>)

    @Query("SELECT * FROM film WHERE id = :id")
    abstract fun findById(id: Int): LiveData<Film>

    @Query("SELECT * FROM film ORDER BY id DESC")
    abstract fun getFilms(): LiveData<List<Film>>

    @Query("SELECT * FROM film ORDER BY vote_average DESC limit 20")
    abstract fun getFilmsTop(): LiveData<List<Film>>

    @Query("SELECT * FROM film ORDER BY popularity DESC limit 20")
    abstract fun getFilmsPop(): LiveData<List<Film>>

    @Query("SELECT * FROM film WHERE title= :title limit 1")
    abstract fun findByTitle(title: String): LiveData<Film>

    @Query("SELECT * FROM film WHERE title LIKE :title")
    abstract fun searchByTitle(title: String): LiveData<List<Film>>

    @Update
    abstract fun updateFilm(vararg film: Film)

    @Delete
    abstract fun deleteFilm(film: Film)

    @Query("DELETE FROM film")
    abstract fun deleteAll()

}