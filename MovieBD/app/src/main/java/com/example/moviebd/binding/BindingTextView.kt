package com.example.moviebd.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("textFloat")
fun TextView.setFloat(float: Float) {
    text = String.format("%.1f", float)
}