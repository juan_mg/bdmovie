package com.example.moviebd.binding

import androidx.databinding.BindingAdapter
import com.example.moviebd.CodingApp
import com.example.moviebd.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

@BindingAdapter("url")
fun loadUrl(iv: CircleImageView, url: String?) {
    if (url == null) {
        iv.setImageDrawable(CodingApp.getContext().getDrawable(R.drawable.ic_launcher_background))
    } else {
        Glide.with(iv).load("https://image.tmdb.org/t/p/original"+url).into(iv)
    }
}
