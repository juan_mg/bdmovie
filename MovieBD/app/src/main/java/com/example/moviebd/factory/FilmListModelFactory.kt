package com.example.moviebd.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviebd.view.adapter.FilmItemAdapter
import com.example.moviebd.viewmodel.FilmListViewModel

class FilmListModelFactory(val listener: FilmItemAdapter.OnFilmClickListener) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FilmListViewModel(listener) as T
    }


}