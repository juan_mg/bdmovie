package com.example.moviebd.view.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.moviebd.model.data.Film
import com.example.moviebd.BR
import com.example.moviebd.viewmodel.FilmListViewModel

class FilmItemAdapter (var viewModel: FilmListViewModel, var resource: Int, val itemClickListener: OnFilmClickListener) :
    RecyclerView.Adapter<FilmItemAdapter.FilmViewHolder>() {
    var list: List<Film>? = null
    interface OnFilmClickListener {
        fun onFilmClick(film: Film, position: Int)
    }
    fun setFilmList(list: List<Film>) {
        this.list = list
        list.forEach { Log.i("TAG", "setFilmList: ${it.title} ") }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmViewHolder {
        var layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        var binding: ViewDataBinding =
            DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return FilmViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FilmViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemCount(): Int = list?.size ?: 0


    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    fun getLayoutIdForPosition(position: Int): Int {
        return resource
    }


    inner class FilmViewHolder(binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var binding: ViewDataBinding? = null

        fun bind(film: FilmListViewModel, position: Int) {
            binding?.setVariable(BR.model, film)
            binding?.setVariable(BR.position, position)
            binding?.root?.setOnClickListener {
                itemClickListener.onFilmClick(film.getFilmAtPosition(position)!!,position)
            }
        }

        init {
            this.binding = binding
        }
    }
}