package com.example.moviebd.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.example.moviebd.R
import com.example.moviebd.databinding.FragmentFilmListBinding
import com.example.moviebd.factory.FilmListModelFactory
import com.example.moviebd.model.data.Film
import com.example.moviebd.view.adapter.FilmItemAdapter
import com.example.moviebd.viewmodel.FilmListViewModel
import com.example.moviebd.viewmodel.SearchViewModel
import com.google.android.material.snackbar.Snackbar

class FilmListFragment : Fragment(), FilmItemAdapter.OnFilmClickListener {
    private var _binding: FragmentFilmListBinding? = null
    private val binding get() = _binding
    private val _viewModel by viewModels<FilmListViewModel> { FilmListModelFactory(this) }
    private val _searchViewModel by activityViewModels<SearchViewModel>()
    private var _TYPE = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFilmListBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        Animatoo.animateCard(activity)
        return binding?.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding?.model = _viewModel
        if (arguments!=null){
            _TYPE = requireArguments().getInt("type")
        }

        getFilmsList()

        binding!!.swipeUpdateList.setOnRefreshListener {
            getFilmsList()
        }
        _viewModel.message?.observe(viewLifecycleOwner, {
            showMessage(it)
        })
        _viewModel.isLoading?.observe(viewLifecycleOwner, {
            showLoading(it)
        })
        _searchViewModel.textSearch.observe(viewLifecycleOwner, { text ->
            _viewModel.getFilmListByName(text,_TYPE).observe(viewLifecycleOwner, {
                if(it.isEmpty()){
                    _viewModel.setFilmsInRecyclerView(_viewModel.films.value?.data)
                }else{
                    _viewModel.setFilmsInRecyclerView(it)
                }
            })
        })

    }

    private fun getFilmsList() {
        _viewModel.getFilmList(_TYPE).observe(viewLifecycleOwner, { resource ->
            _viewModel.setFilmsInRecyclerView(resource.data)
        })
    }

    override fun onFilmClick(film: Film, position: Int) {
        val typeList = arguments?.getString("list")
        val bundle = bundleOf("film" to film, "list" to typeList)
        if (typeList.equals("top")) {
            NavHostFragment.findNavController(this)
                .navigate(R.id.detail_film_fragment, bundle)
        } else {
            NavHostFragment.findNavController(this)
                .navigate(R.id.detail_film_fragment, bundle)
        }
    }


    fun showMessage(message: String) {
        val snack = Snackbar.make(_binding!!.rvFilmList, message, Snackbar.LENGTH_LONG)
        snack.setAction(R.string.accept) {
            snack.dismiss()
        }
        snack.show()
    }

    fun showLoading(isLoading: Boolean) {
        binding!!.swipeUpdateList.isRefreshing = isLoading
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        private const val TAG = "FilmDetailFragment"
    }
}