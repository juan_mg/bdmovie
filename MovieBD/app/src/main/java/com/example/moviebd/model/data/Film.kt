package com.example.moviebd.model.data

import androidx.room.Entity
import java.io.Serializable


@Entity(primaryKeys = ["id"])
data class Film(
    val overview: String? = null,
    val original_language: String? = null,
    val original_title: String? = null,
    val video: Boolean? = null,
    val title: String? = null,
    val poster_path: String? = null,
    val backdrop_path: String? = null,
    val release_date: String? = null,
    val popularity: Double? = null,
    val vote_average: Float? = null,
    val id: Int? = null,
    val adult: Boolean? = null,
    val vote_count: Int? = null,
    var is_top: Boolean?=false
):Serializable{

    override fun toString(): String {
        return "Film(overview=$overview, original_language=$original_language, original_title=$original_title, video=$video, title=$title, poster_path=$poster_path, backdrop_path=$backdrop_path, release_date=$release_date, popularity=$popularity, vote_average=$vote_average, id=$id, adult=$adult, vote_count=$vote_count, is_top=$is_top)"
    }
}

