package com.example.moviebd.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviebd.model.data.Film

@Database(entities = [Film::class], version = FilmDB.VERSION)
abstract class FilmDB : RoomDatabase(){
    abstract fun filmDao(): FilmDao
    companion object{
        const val VERSION = 2
    }
}
