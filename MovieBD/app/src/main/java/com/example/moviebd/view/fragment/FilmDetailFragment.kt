package com.example.moviebd.view.fragment

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.example.moviebd.R
import com.example.moviebd.databinding.FragmentFilmDetailBinding
import com.example.moviebd.model.data.Film
import com.google.android.youtube.player.YouTubeIntents

class FilmDetailFragment : Fragment() {
    private var _binding: FragmentFilmDetailBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Animatoo.animateCard(activity)
        return inflater.inflate(R.layout.fragment_film_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Animatoo.animateCard(activity)
        val film = arguments?.getSerializable("film") as Film
        Log.i(TAG, "onViewCreated: ${film.title}")

        Glide.with(this).load("https://image.tmdb.org/t/p/w500" + film.poster_path)
            .into(view.findViewById(R.id.iv_film_image))

        Glide.with(this).load("https://image.tmdb.org/t/p/original" + film.backdrop_path)

            .into(view.findViewById(R.id.iv_film_bg))
        view.findViewById<TextView>(R.id.tv_name_film)
            .setText("Titulo: ${film.title}")

        var language: String = ""
        when (film.original_language) {
            "en" -> language = "Ingles"
            "es" -> language = "Español Castellano"
            "lat" -> language = "Español Latino"
            else -> { // Note the block
                language = "No identificado"
            }
        }
        view.findViewById<TextView>(R.id.tv_language_film)
            .setText("Idioma : ${language}")

        view.findViewById<TextView>(R.id.tv_votes_film)
            .setText("Promedio de ${film.vote_average}/10 con ${film.vote_count} votos")

        view.findViewById<TextView>(R.id.tv_description_film).setText("${film.overview}")

        view.findViewById<RatingBar>(R.id.rate_film).rating = film.vote_average!!


        view.findViewById<LinearLayout>(R.id.ll_yt)
            .setOnClickListener {
                view.findViewById<LinearLayout>(R.id.animationView).visibility = View.VISIBLE
                Handler().postDelayed({
                    view.findViewById<LinearLayout>(R.id.animationView).visibility = View.INVISIBLE
                    startActivity(
                        YouTubeIntents.createSearchIntent(
                            activity,
                            film.title + " Official trailer in spanish"
                        )
                    )
                    Animatoo.animateCard(activity)
                }, 3000)

            }


        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    val typeList = arguments?.getString("list")
//                    if (typeList.equals("top")) {
//                        findNavController().navigate(R.id.action_DetailFragment_to_TopRateFragment)
//                    }else{
//                        findNavController().navigate(R.id.action_DetailFragment_to_PopularFragment)
//                    }
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
//
    }


    companion object {
        private const val TAG = "FilmDetailFragment"
    }

}