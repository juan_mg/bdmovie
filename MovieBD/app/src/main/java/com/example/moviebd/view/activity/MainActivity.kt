package com.example.moviebd.view.activity

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.example.moviebd.R
import com.example.moviebd.databinding.ActivityMainBinding
import com.example.moviebd.factory.SearchModelFactory
import com.example.moviebd.viewmodel.SearchViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    lateinit var binding : ActivityMainBinding
    private val _searchViewModel by viewModels<SearchViewModel>{SearchModelFactory()}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Animatoo.animateCard(this)
        }

        setSupportActionBar(findViewById(R.id.toolbar))

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupActionBarWithNavController(this, navController)

        binding.contentMain.filmsBottomNavigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_top_rate -> {
                    val bundle = Bundle()
                    bundle.putInt("type", 0)
                    navController.navigate(R.id.films_list_fragment, bundle)
                }
                R.id.action_popularity -> {
                    val bundle = Bundle()
                    bundle.putInt("type", 1)
                    navController.navigate(R.id.films_list_fragment,bundle)
                }
            }
            true
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val item = menu?.findItem(R.id.search)
        val searchView = SearchView(this.supportActionBar!!.themedContext)
        item!!.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItem.SHOW_AS_ACTION_IF_ROOM)
        item.setActionView(searchView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { _searchViewModel.search(it) }
                Log.i("TAG","SEARCH")
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
        searchView.setOnClickListener {
            Log.i("TAG", "onCreateOptionsMenu: ")
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_logout){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}