package com.example.moviebd

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.moviebd.viewmodel.SearchViewModel
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1], application = CodingApp::class)
class SearchViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()
    val viewModel = SearchViewModel()

    @Test
    fun testExistFilm() {
        val observer = object : Observer<Any> {
            override fun onChanged(itIsReal: Any) {
                if (itIsReal == true) {
                    print("Hay similitudes")
                }
            }
        }

        //Titulo
        viewModel.search("A")
        viewModel.textSearch?.observeForever(observer)

    }

}